#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from chime device
$(call inherit-product, device/xiaomi/chime/device.mk)

PRODUCT_DEVICE := chime
PRODUCT_NAME := lineage_chime
PRODUCT_BRAND := Xiaomi
PRODUCT_MODEL := Redmi 9T
PRODUCT_MANUFACTURER := xiaomi

PRODUCT_GMS_CLIENTID_BASE := android-google

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="arrow_chime-userdebug 12 SQ3A.220605.009.B1 1656338943 release-keys"

BUILD_FINGERPRINT := Xiaomi/arrow_chime/chime:12/SQ3A.220605.009.B1/buxx06271406:userdebug/release-keys
